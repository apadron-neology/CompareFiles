﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CompareFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            string oldFile = "c:/hotfiles/old.txt";
            string newFile = "c:/hotfiles/new.txt";
            FileProcessor fileProcessor = new FileProcessor();
            List<string> file1 = fileProcessor.getListHashes(oldFile);
            List<string> file2 = fileProcessor.getListHashes(newFile);
            List<string> eliminados = file1.Except(file2).ToList();
            List<string> nuevos = file2.Except(file1).ToList();
            file1.Clear();
            file2.Clear();
            GC.Collect();
            Console.ReadLine();
        }

    }
    class FileProcessor{
        public List<string> getListHashes(string fileName)
        {
            Console.WriteLine("Reading each line into string array. Process with Parallel.For: ");
            DateTime start = DateTime.Now;
            DateTime end;
            List<string> AllLines = new List<string>();
            try
            {
                using (StreamReader sr = File.OpenText(fileName))
                {
                    int x = 0;
                    while (!sr.EndOfStream)
                    {
                        AllLines.Add(sr.ReadLine());
                        x += 1;
                    }
                } //CLOSE THE FILE because we are now DONE with it.
                //Parallel.For(0, AllLines.Count, x =>
                //{
                //    AllLines[x] = HashString(AllLines[x]); //to simulate work
                //});
                end = DateTime.Now;
                Console.WriteLine("Finished at: " + end.ToLongTimeString());
                Console.WriteLine("Time: " + (end - start));
                Console.WriteLine();
            }
            catch (OutOfMemoryException)
            {
                end = DateTime.Now;
                Console.WriteLine("Not enough memory. Couldn't perform this test.");
                Console.WriteLine("Finished at: " + end.ToLongTimeString());
                Console.WriteLine("Time: " + (end - start));
                Console.WriteLine();
            }
            catch (Exception)
            {
                end = DateTime.Now;
                Console.WriteLine("EXCEPTION. Couldn't perform this test.");
                Console.WriteLine("Finished at: " + end.ToLongTimeString());
                Console.WriteLine("Time: " + (end - start));
                Console.WriteLine();
            }
            GC.Collect();
            Thread.Sleep(1000);
            return AllLines;
        }

        private string HashString(string text)
        {
            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                return BitConverter.ToString(
                    md5.ComputeHash(
                        System.Text.Encoding.UTF8.GetBytes(text))).Replace("-", "");
            }
        }
    }
}
